package com.example.glean

class EventHelper {

    companion object {
        fun resolveEvents(remote: Event, local: Event): Event {

            val returnEvent = Event()
            returnEvent.id = remote.id

            if(remote.name.equals(local.name)){
                returnEvent.name = remote.name
            }
            else{
                val mergedName = mergeText(local.name!!, remote.name!!)
                returnEvent.name = mergedName
            }

            //Loop through remote notes compare to local notes
            for(ln:Note in local.notes){
                var hasNote = false
                for(rn:Note in remote.notes){
                    if(rn.id == ln.id){
                        if(rn.text.equals(ln.text)){
                            //notes are the same add note to return event and move onto next note
                            returnEvent.notes.add(rn)
                            hasNote = true
                            break
                        }
                        else{
                            //notes are not the same, needs merge
                            val mergedNote = Note()
                            val mergedNoteText = mergeText(ln.text!!, rn.text!!)
                            mergedNote.id = rn.id
                            mergedNote.text = mergedNoteText
                            returnEvent.notes.add(mergedNote)
                            hasNote = true
                            break
                        }
                    }
                }
                if(!hasNote){
                    //if hasNote is false add the note
                    returnEvent.notes.add(ln)
                }
            }

            //Check if any remote notes are missing
            for(rn:Note in remote.notes) {
                for (ln: Note in local.notes) {
                    if(rn.id == ln.id){
                        //remote and local have already been compared and resolved
                        break
                    }
                }
                returnEvent.notes.add(rn)
            }
            return returnEvent
        }

        fun mergeText(s1:String, s2:String):String{
            return s1 + " / " + s2
        }

    }
}