package com.example.glean

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun mergeText_isCorrect() {
        assertEquals("hello / world", EventHelper.mergeText("hello", "world"))
    }

    @Test
    fun mergeEventsNotesCount_isCorrect() {
        var e1:Event = Event()
        var e2:Event = Event()
        var n1:Note = Note()
        var n2:Note = Note()

        e1.id = 1
        e2.id = 1

        e1.name = "Name 1"
        e2.name = "Name 2"

        n1.id = 1
        n1.text = "A"

        n2.id = 2
        n2.text = "B"

        e1.notes.add(n1)
        e2.notes.add(n2)

        assertEquals( 2, EventHelper.resolveEvents(e2, e1).notes!!.size)
    }

    @Test
    fun mergeEventsName_isCorrect() {
        var e1:Event = Event()
        var e2:Event = Event()
        var n1:Note = Note()
        var n2:Note = Note()

        e1.id = 1
        e2.id = 1

        e1.name = "Name 1"
        e2.name = "Name 2"

        n1.id = 1
        n1.text = "A"

        n2.id = 2
        n2.text = "B"

        e1.notes.add(n1)
        e2.notes.add(n2)

        assertEquals( "Name 1 / Name 2", EventHelper.resolveEvents(e2, e1).name)
        assertEquals( "A", EventHelper.resolveEvents(e2, e1).notes[0].text)
        assertEquals( "B", EventHelper.resolveEvents(e2, e1).notes[1].text)
        assertEquals( 1, EventHelper.resolveEvents(e2, e1).notes[0].id)
        assertEquals( 2, EventHelper.resolveEvents(e2, e1).notes[1].id)
    }

}